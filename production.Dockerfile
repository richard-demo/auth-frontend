FROM node:18.3.0-buster AS builder

# Fix for JS heap limit allocation issue
ENV NODE_OPTIONS="--max-old-space-size=4096"

RUN mkdir -p /app
WORKDIR /app

# Scripts for building
ADD ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock

RUN yarn install

COPY . .

ENV NODE_ENV=production

RUN yarn run build

FROM --platform=linux/x86_64 nginx:stable-alpine as production-stage
COPY --from=builder /app/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
