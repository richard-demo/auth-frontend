import { Box, Card, CardContent, CardHeader } from '@mui/material'
import { styled } from '@mui/system'

export const CardStyled = styled(Card)`
  border: none;
`
export const CardHeaderStyled = styled(CardHeader)`
  padding: ${({ theme }) => `${theme.spacing(4)} ${theme.spacing(2)}`};
  border-bottom: 1px solid ${({ theme }) => theme.palette.border.light as string};
  .MuiCardHeader-title {
    font-weight: 700;
    text-align: center;
  }
`

export const CardContentStyled = styled(CardContent)`
  padding: 0 !important;
`

export const CardRowStyled = styled(Box)`
  width: 100%;
  padding: ${({ theme }) => `${theme.spacing(4)} ${theme.spacing(7)}`};

  ${({ theme }) => theme.breakpoints.down('md')} {
    padding: ${({ theme }) => theme.spacing(2)};
  }
`
