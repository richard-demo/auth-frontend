import { CardContentStyled, CardHeaderStyled, CardStyled } from './styles'

interface AuthCardProps {
  title?: string
}

const AuthCard: React.FC<React.PropsWithChildren<AuthCardProps>> = ({ title, children }) => {
  return (
    <CardStyled>
      {title && <CardHeaderStyled title={title} titleTypographyProps={{ variant: 'h4' }} />}

      <CardContentStyled>{children}</CardContentStyled>
    </CardStyled>
  )
}

export default AuthCard
