import { useEffect, useState } from 'react'
import { Navigate, useLocation } from 'react-router-dom'
import { authService } from 'services/auth.service'

export const AuthRoute: React.FC<React.PropsWithChildren<{ notLoggedInRequired?: boolean }>> = ({ children, notLoggedInRequired }) => {
  const [session, setSession] = useState(authService.currentSessionValue)
  const location = useLocation()

  useEffect(() => {
    const subject = authService.currentSession.subscribe(newSession => {
      setSession(newSession)
    })

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return () => subject.unsubscribe()
  }, [])

  if (!session.isLogged && !notLoggedInRequired)
    return (
      <Navigate
        to={{
          pathname: '/sign-in',
          search: `?redirectTo=${location.pathname}`
        }}
        replace
      />
    )

  if (session.isLogged && notLoggedInRequired)
    return (
      <Navigate
        to={{
          pathname: '/dashboard'
        }}
        replace
      />
    )

  return <>{children}</>
}
