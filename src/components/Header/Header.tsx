import { Box, Button } from '@mui/material'

import { ContainerStyled, HeaderWrapperStyled } from './styles'
import { useEffect, useMemo, useState } from 'react'
import { authService } from 'services/auth.service'
const Header = () => {
  const [session, setSession] = useState(authService.currentSessionValue)

  useEffect(() => {
    const subject = authService.currentSession.subscribe(newSession => {
      setSession(newSession)
    })

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return () => subject.unsubscribe()
  }, [])



  const renderRightActions = useMemo(() => {
    let el
    if (session.isLogged) {
      el = <Button onClick={() => authService.logout()}>Logout</Button>
    }

    return el
  }, [session.isLogged])

  return (
    <HeaderWrapperStyled>
      <ContainerStyled>
        <h3>Logo</h3>
        <Box>
          {renderRightActions}
        </Box>
      </ContainerStyled>
    </HeaderWrapperStyled>
  )
}

export default Header
