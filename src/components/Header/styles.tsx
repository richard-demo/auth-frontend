import { Paper } from '@mui/material'

import styledE from '@emotion/styled'

import { styled } from '@mui/system'

export const HeaderWrapperStyled = styledE.header`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  z-index: 1201;

`

export const ContainerStyled = styled(Paper)`
  height: 52px;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  position: relative;
  border-radius: 0 !important;
  background-color: ${({ theme }) => theme.palette['background'].paper as string};
  border-bottom: 1px solid ${({ theme }) => theme.palette['border'].light as string};
  padding: 0 ${({ theme }) => theme.spacing(5)};
  justify-content: space-between;
`
