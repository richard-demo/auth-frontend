import type { TextFieldProps } from '@mui/material'
import { TextField } from '@mui/material'
import get from 'lodash/get'
import { useMemo } from 'react'
import type { FieldError, Validate } from 'react-hook-form'
import { useFormContext } from 'react-hook-form'

const TextInput = ({
  name,
  validate,
  required = false,
  pattern,
  minLength,
  maxLength = 50,
  ...props
}: Omit<TextFieldProps, 'error' | 'variant' | 'ref' | 'fullWidth'> & {
  name: string
  validate?: Validate<string, any>
  required?: boolean
  pattern?: RegExp
  minLength?: number
  maxLength?: number
}) => {
  const { register, formState } = useFormContext() || {}
  const fieldError = get(formState.errors, name) as FieldError | undefined

  const errorInLabel = useMemo(() => {
    switch (fieldError?.type) {
      case 'maxLength':
        return `Maximum ${maxLength} symbols`
      case 'minLength':
        return minLength ? `Minimum ${minLength} symbols` : props.label

      default:
        return fieldError?.message
    }
  }, [fieldError?.message, fieldError?.type, maxLength, minLength, props.label])

  return (
    <TextField
      {...props}
      variant="outlined"
      label={<>{errorInLabel || props.label}</>}
      error={Boolean(fieldError)}
      fullWidth
      required={required}
      {...register(name, {
        pattern,
        maxLength,
        minLength,
        required,
        validate: validate ? (value, cur) => validate(value, cur) : undefined
      })}
    />
  )
}

export default TextInput
