import { Header } from 'components/Header'
import { MainStyled } from './styles'
const PageLayout: React.FC<React.PropsWithChildren> = ({ children }) => {
  return (
    <>
      <Header />
      <MainStyled>{children}</MainStyled>
    </>
  )
}

export default PageLayout
