import styledE from '@emotion/styled'

export const MainStyled = styledE.main`
    padding-top: 52px;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
`