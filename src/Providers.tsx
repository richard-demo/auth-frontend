import React, { ReactNode } from 'react'
import type { Theme } from '@mui/material/styles'
import { ThemeProvider } from '@mui/material/styles'

import { useDarkMode } from 'hooks/useDarkMode'
import AppThemeProvider from 'theme/ThemeProvider'

const Providers = ({ children }: { children: ReactNode | ReactNode[] }) => {
  const isDarkMode = useDarkMode()
  const themeMode = isDarkMode ? 'dark' : 'light'

  return <AppThemeProvider mode={themeMode}>{(theme: Theme) => <ThemeProvider theme={theme}>{children}</ThemeProvider>}</AppThemeProvider>
}

export default Providers
