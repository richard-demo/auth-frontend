import React from 'react'
import { PaletteMode, Theme, ThemeProvider } from '@mui/material'
import createAppTheme from './theme'

type AppThemeProviderProps = {
  children: (theme: Theme) => React.ReactNode
  mode: PaletteMode
}

const AppThemeProvider: React.FC<AppThemeProviderProps> = ({ children, mode }) => {
  const theme = React.useMemo(() => createAppTheme(mode), [mode])

  return <ThemeProvider theme={theme}>{children(theme)}</ThemeProvider>
}

export default AppThemeProvider
