export interface SignInResponse {
  firstName?: string
  lastName?: string
  email?: string
  token?: string
}
