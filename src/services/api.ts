import axios from 'axios'
import { authService } from './auth.service'

export class ResponseError extends Error {
  code: number
  constructor(message: string, code: number) {
    super(message)
    this.code = code
  }
}

export const axiosInstance = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? '/api' : process.env.REACT_APP_API_BASE_URL ?? 'http://localhost:8000/api',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
  validateStatus: status => status < 300
})

axiosInstance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    const token = authService.currentSessionValue.token
    if (token && typeof token === 'string') config.headers.setAuthorization(`Bearer ${token}`)
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  resp => resp,
  async error => {
    if (error?.response?.data?.message) {
      const errors = error.response.data.message
      if (authService.currentSessionValue.isLogged && error.response.data?.statusCode === 401) {
        authService.logout()
      }
      if (typeof errors === 'string') throw new ResponseError(errors, error.response.data.statusCode)

      // handle error for class validator
      if (Array.isArray(errors) && errors.length > 0 && errors[0]?.constraints) {
        const constraintKeys = Object.keys(errors[0]?.constraints)
        if (constraintKeys.length > 0) throw new ResponseError(errors[0]?.constraints[constraintKeys[0]], error.response.data.statusCode)
      }
    }

    if (error?.response) console.error('axios fetch error', error?.response)

    return Promise.reject(error)
  }
)
