import { BehaviorSubject } from 'rxjs'
import { axiosInstance } from './api'
import { SignInResponse } from './types'

export interface ICurrentSessionSubject {
  firstName: string
  lastName: string
  email: string
  token: string
  isLogged: boolean
}

const currentToken = localStorage.getItem('auth_token')

const initialSession = {
  firstName: '',
  lastName: '',
  email: '',
  token: '',
  isLogged: false
}

const currentSessionSubject = new BehaviorSubject<ICurrentSessionSubject>({
  ...initialSession,
  token: currentToken ?? '',
  isLogged: !!currentToken
})

export const authService = {
  currentSession: currentSessionSubject.asObservable(),
  get currentSessionValue() {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return currentSessionSubject.value
  },
  updateCurrentSession(data: ICurrentSessionSubject) {
    currentSessionSubject.next(data)
  },
  fetchSignIn,
  fetchSignUp,
  fetchValidate,
  logout
}

async function fetchSignIn(email: string, password: string): Promise<SignInResponse> {
  return (await axiosInstance.post<SignInResponse>('/auth/sign-in', { email, password })).data
}

async function fetchSignUp(firstName: string, lastName: string, email: string, password: string): Promise<SignInResponse> {
  return (await axiosInstance.post<SignInResponse>('/auth/sign-up', { firstName, lastName, email, password })).data
}

async function fetchValidate(): Promise<SignInResponse> {
  return (await axiosInstance.get<SignInResponse>('/auth/validate')).data
}

function logout() {
  authService.updateCurrentSession(initialSession)
  localStorage.removeItem('auth_token')
}
