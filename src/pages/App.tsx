import { Navigate, Route, Routes } from 'react-router-dom'

import SignIn from './SignIn'
import SignUp from './SignUp'
import Dashboard from './Dashboard'
import { useEffect } from 'react'
import { authService } from 'services/auth.service'
import { AuthRoute } from 'components/AuthRoute'
import { PageLayout } from 'components/PageLayout'

export default function App() {
  useEffect(() => {
    async function fetchSessionValidate() {
      try {
        const userValidate = await authService.fetchValidate()
        authService.updateCurrentSession({ ...authService.currentSessionValue, ...userValidate })
      } catch (err) {
        console.error('validate cal', err)
      }
    }
    if (authService.currentSessionValue.isLogged) fetchSessionValidate()
  }, [])

  return (
    <PageLayout>
      <Routes>
        <Route path="/" element={<Navigate to="/sign-in" />} />
        <Route
          path="/sign-in"
          element={
            <AuthRoute notLoggedInRequired>
              <SignIn />
            </AuthRoute>
          }
        />
        <Route
          path="/sign-up"
          element={
            <AuthRoute notLoggedInRequired>
              <SignUp />
            </AuthRoute>
          }
        />
        <Route
          path="/dashboard"
          element={
            <AuthRoute>
              <Dashboard />
            </AuthRoute>
          }
        />
      </Routes>
    </PageLayout>
  )
}
