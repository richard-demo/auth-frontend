import { Container, Grid, Typography, Box, Divider, Link as MUILink, Alert, IconButton, AlertColor } from '@mui/material'
import { LoadingButton } from '@mui/lab'
import CloseIcon from '@mui/icons-material/Close'
import { FormProvider, useForm } from 'react-hook-form'
import { Link } from 'react-router-dom'
import TextInput from 'components/TextInput'
import { AuthCard, CardRowStyled } from 'components/AuthCard'

import { SignInForm, SignInFormFields } from './types'
import { useSignInSubmit } from './useSignInSubmit'
import { useMemo } from 'react'

const AlertResult = ({ msg, severity, onClose }: { msg: string; severity: AlertColor; onClose?: () => any }) => {
  return (
    <Box mt={3}>
      <Alert
        severity={severity}
        action={
          onClose && (
            <IconButton aria-label="close" color="inherit" size="small" onClick={onClose}>
              <CloseIcon fontSize="inherit" />
            </IconButton>
          )
        }
        sx={{ mb: 2 }}
      >
        {msg}
      </Alert>
    </Box>
  )
}

export default function SignIn() {
  const formMethods = useForm<SignInForm>({
    mode: 'all',
    defaultValues: {
      [SignInFormFields.email]: '',
      [SignInFormFields.password]: ''
    }
  })

  const {
    handleSubmit,
    reset,
    formState: { errors, isValid }
  } = formMethods

  const { loading, error, data, onSubmit, onReset } = useSignInSubmit({
    onSuccess: () => {
      reset()
    }
  })

  const renderAlerts = useMemo(() => {
    let alerts = null
    if (error) alerts = <AlertResult severity="error" msg={error} onClose={onReset} />
    else if (data.firstName && data.lastName)
      alerts = (
        <AlertResult
          severity="success"
          msg={`Signed in successfully, Welcome ${data.firstName} ${data.lastName} to Application Demo, Redirecting...`}
        />
      )
    return alerts
  }, [error, data.firstName, data.lastName, onReset])

  return (
    <Container>
      <Grid container columnSpacing={3} justifyContent="center" mt={[2, null, 7]}>
        <Grid item xs={12} md={8}>
          <AuthCard title="Sign In To Continue">
            <FormProvider {...formMethods}>
              <form onSubmit={handleSubmit(onSubmit)} id="sign-in-form">
                <CardRowStyled>
                  <Box mb={4}>
                    <TextInput
                      required
                      name={SignInFormFields.email}
                      label={errors?.[SignInFormFields.email]?.message || 'Email'}
                      placeholder="Enter your email"
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        type: 'email'
                      }}
                    />
                  </Box>
                  <Box mb={4}>
                    <TextInput
                      required
                      name={SignInFormFields.password}
                      label={errors?.[SignInFormFields.password]?.message || 'Password'}
                      placeholder="Enter your password"
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        type: 'password'
                      }}
                    />
                  </Box>

                  <Typography>
                    By continuing you consent to the{' '}
                    <MUILink>terms of use</MUILink>
                    {' '}
                    and{' '}
                    <MUILink>privacy policy</MUILink>
                    .
                  </Typography>
                </CardRowStyled>
                <Divider />

                <CardRowStyled>
                  <LoadingButton loading={loading} type="submit" variant="contained" fullWidth disabled={!isValid}>
                    Sign In
                  </LoadingButton>
                  {renderAlerts}
                  <Grid mt={3} justifyContent={'space-between'} container>
                    <Grid item sm={6} xs={12} mb={[1]}>
                      <MUILink href="#" variant="body2">
                        Forgot password?
                      </MUILink>
                    </Grid>
                    <Grid item>
                      <MUILink component={Link} to="/sign-up" variant="body2">
                        {"Don't have an account? Sign Up"}
                      </MUILink>
                    </Grid>
                  </Grid>
                </CardRowStyled>
              </form>
            </FormProvider>
          </AuthCard>
        </Grid>
      </Grid>
    </Container>
  )
}
