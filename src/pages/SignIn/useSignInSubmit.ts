import { useCallback, useMemo, useState } from 'react'
import { SignInForm } from './types'
import { authService } from 'services/auth.service'
import { SignInResponse } from 'services/types'
import queryString from 'query-string'
import { useLocation, useNavigate } from 'react-router-dom'

export const useSignInSubmit = (options?: { onSuccess: (data: SignInResponse) => any }) => {
  const [loading, setLoading] = useState(false)

  const location = useLocation()
  const navigate = useNavigate()

  const [error, setError] = useState('')

  const [data, setData] = useState<SignInResponse>({})

  const onSubmit = useCallback(
    async (payload: SignInForm) => {
      try {
        setLoading(true)
        setError('')
        setData({})

        const data = await authService.fetchSignIn(payload.email, payload.password)
        setData(data)
        if (options?.onSuccess) options.onSuccess(data)

        setTimeout(() => {
          authService.updateCurrentSession({ ...authService.currentSessionValue, ...data, isLogged: true })

          const params = queryString.parse(location.search)
          const to = (params.redirectTo && params.redirectTo.toString()) || '/dashboard'
          navigate(to)
        }, 2000)

        localStorage.setItem('auth_token', data.token!)
      } catch (error) {
        if (error instanceof Error) setError(error.message)
        else {
          console.error('Unknown error sign in submit', error)
        }
      } finally {
        setLoading(false)
      }
    },
    [options, location.search, navigate]
  )

  const onReset = useCallback(() => {
    setLoading(false)
    setError('')
    setData({})
  }, [])

  return useMemo(() => ({ loading, error, data, onSubmit, onReset }), [loading, error, data, onSubmit, onReset])
}
