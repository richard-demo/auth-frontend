export type SignInForm = {
  email: string
  password: string
}

export enum SignInFormFields {
  email = 'email',
  password = 'password'
}
