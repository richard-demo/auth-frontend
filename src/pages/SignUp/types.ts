export type SignUpForm = {
  firstName: string
  lastName: string
  email: string
  password: string
}

export enum SignUpFormFields {
  firstName = 'firstName',
  lastName = 'lastName',
  email = 'email',
  password = 'password'
}
