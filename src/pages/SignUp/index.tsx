import { Container, Grid, Typography, Divider, Link as MUILink, AlertColor, Alert, IconButton, Box } from '@mui/material'
import { LoadingButton } from '@mui/lab'
import CloseIcon from '@mui/icons-material/Close'
import { FormProvider, useForm } from 'react-hook-form'
import { Link } from 'react-router-dom'
import TextInput from 'components/TextInput'
import { AuthCard, CardRowStyled } from 'components/AuthCard'
import { SignUpForm, SignUpFormFields } from './types'
import { useSignUpSubmit } from './useSignUpSubmit'
import { useMemo } from 'react'

const AlertResult = ({ msg, severity, onClose }: { msg: string; severity: AlertColor; onClose?: () => any }) => {
  return (
    <Box mt={3}>
      <Alert
        severity={severity}
        action={
          onClose && (
            <IconButton aria-label="close" color="inherit" size="small" onClick={onClose}>
              <CloseIcon fontSize="inherit" />
            </IconButton>
          )
        }
        sx={{ mb: 2 }}
      >
        {msg}
      </Alert>
    </Box>
  )
}

export default function SignUp() {
  const formMethods = useForm<SignUpForm>({
    mode: 'all'
  })

  const {
    handleSubmit,
    formState: { errors, isValid },
    reset
  } = formMethods

  const { loading, error, data, onSubmit, onReset } = useSignUpSubmit({
    onSuccess: () => {
      reset()
    }
  })

  const renderAlerts = useMemo(() => {
    let alerts = null
    if (error) alerts = <AlertResult severity="error" msg={error} onClose={onReset} />
    else if (data.firstName && data.lastName)
      alerts = (
        <AlertResult
          severity="success"
          msg={`Signed up successfully, Welcome ${data.firstName} ${data.lastName} to Application Demo, Redirecting...`}
        />
      )
    return alerts
  }, [error, data.firstName, data.lastName, onReset])


  return (
    <Container>
      <Grid container columnSpacing={3} justifyContent="center" mt={[2, null, 7]}>
        <Grid item xs={12} md={8}>
          <AuthCard title="Create New Account">
            <FormProvider {...formMethods}>
              <form onSubmit={handleSubmit(onSubmit)} id="sign-up-form">
                <CardRowStyled>
                  <Grid container spacing={4}>
                    <Grid item xs={12} sm={6}>
                      <TextInput
                        minLength={3}
                        required
                        name={SignUpFormFields.firstName}
                        label={errors?.[SignUpFormFields.firstName]?.message || 'First Name'}
                        placeholder="Enter your first name"
                        InputLabelProps={{ shrink: true }}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextInput
                        minLength={3}
                        required
                        name={SignUpFormFields.lastName}
                        label={errors?.[SignUpFormFields.lastName]?.message || 'Last Name'}
                        placeholder="Enter your last name"
                        InputLabelProps={{ shrink: true }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextInput
                        required
                        name={SignUpFormFields.email}
                        label={errors?.[SignUpFormFields.email]?.message || 'Email'}
                        placeholder="Enter your email"
                        InputLabelProps={{ shrink: true }}
                        InputProps={{
                          type: 'email'
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextInput
                        required
                        name={SignUpFormFields.password}
                        label={errors?.[SignUpFormFields.password]?.message || 'Password'}
                        placeholder="Enter your password"
                        InputLabelProps={{ shrink: true }}
                        InputProps={{
                          type: 'password'
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>
                        By continuing you consent to the{' '}
                        <MUILink>terms of use</MUILink>
                        {' '}
                        and{' '}
                        <MUILink>privacy policy</MUILink>

                        .
                      </Typography>
                    </Grid>
                  </Grid>
                </CardRowStyled>
                <Divider />

                <CardRowStyled>
                  <LoadingButton loading={loading} type="submit" variant="contained" fullWidth disabled={!isValid}>
                    Sign Up
                  </LoadingButton>
                  {renderAlerts}
                  <Grid mt={3} justifyContent={'space-between'} container>
                    <Grid item sm={6} xs={12} mb={[1]}>
                      <MUILink href="#" variant="body2">
                        Forgot password?
                      </MUILink>
                    </Grid>
                    <Grid item>
                      <MUILink component={Link} to="/sign-in" variant="body2">
                        {'Already have an account? Sign in'}
                      </MUILink>
                    </Grid>
                  </Grid>
                </CardRowStyled>
              </form>
            </FormProvider>
          </AuthCard>
        </Grid>
      </Grid>
    </Container>
  )
}
