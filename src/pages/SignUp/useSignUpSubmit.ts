import { useCallback, useMemo, useState } from 'react'
import { SignUpForm } from './types'
import { authService } from 'services/auth.service'
import { SignInResponse } from 'services/types'

export const useSignUpSubmit = (options?: { onSuccess: (data: SignInResponse) => any }) => {
  const [loading, setLoading] = useState(false)

  const [error, setError] = useState('')

  const [data, setData] = useState<SignInResponse>({})

  const onSubmit = useCallback(
    async (payload: SignUpForm) => {
      try {
        setLoading(true)
        setError('')
        setData({})

        const data = await authService.fetchSignUp(payload.firstName, payload.lastName, payload.email, payload.password)
        setData(data)
        if (options?.onSuccess) options.onSuccess(data)

        setTimeout(() => {
          authService.updateCurrentSession({ ...authService.currentSessionValue, ...data, isLogged: true })
        }, 2000)

        localStorage.setItem('auth_token', data.token!)
      } catch (error) {
        if (error instanceof Error) setError(error.message)
        else {
          console.error('Unknown error sign up submit', error)
        }
      } finally {
        setLoading(false)
      }
    },
    [options]
  )

  const onReset = useCallback(() => {
    setLoading(false)
    setError('')
    setData({})
  }, [])

  return useMemo(() => ({ loading, error, data, onSubmit, onReset }), [loading, error, data, onSubmit, onReset])
}
